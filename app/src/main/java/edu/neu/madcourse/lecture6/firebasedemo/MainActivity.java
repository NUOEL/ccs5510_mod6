package edu.neu.madcourse.lecture6.firebasedemo;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;

import edu.neu.madcourse.lecture6.firebasedemo.fcm.FCMActivity;
import edu.neu.madcourse.lecture6.firebasedemo.realtimedatabase.RealtimeDatabaseActivity;

public class MainActivity extends AppCompatActivity {

    private static String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void openFCMActivity(View view) {
        startActivity(new Intent(MainActivity.this, FCMActivity.class));
    }

    public void openDBActivity(View view) {
        startActivity(new Intent(MainActivity.this, RealtimeDatabaseActivity.class));
    }

    public void openSendNotificationActivity(View view){
        startActivity(new Intent(MainActivity.this, SendNotificationActivity.class));
    }

    public void openPermissionsActivity(View view){
        startActivity(new Intent(MainActivity.this, ShowPermActivity.class));
    }


}
